import "package:test/test.dart";
import "package:algorithms/fizz_buzz.dart";

void main() {
  test(
      "should return 'fizz' at multiples of 3, 'buzz' at multiples of 5 and 'fizzbuzz' at multiples of 3 and 5",
      () {
    List<String> ceiling = [
      'FizzBuzz',
      '1',
      '2',
      'Fizz',
      '4',
      'Buzz',
      'Fizz',
      '7',
      '8',
      'Fizz',
      'Buzz',
      '11',
      'Fizz',
      '13',
      '14',
      'FizzBuzz',
      '16',
      '17',
      'Fizz',
      '19'
    ];
    expect(fizzBuzz(20), equals(ceiling));
  });
}

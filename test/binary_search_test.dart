import "package:test/test.dart";
import "package:algorithms/binary_search.dart";

void main() {
  List<int> arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  test("should return -1 if element doesn't exists", () {
    expect(binarySearch(arr, 10), equals(-1));
  });

  test("should return 2", () {
    expect(binarySearch(arr, 3), equals(2));
  });
}

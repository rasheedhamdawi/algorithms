import "package:test/test.dart";
import "package:algorithms/harmless_ransom_note.dart";

void main() {
  String noteText = "software industry unchanged";
  String MagazineText = """
    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
    Lorem Ipsum has been the industry standard dummy text ever since the 1500s,
    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    It has survived not only five centuries, but also the leap into electronic typesetting,
    remaining essentially unchanged It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
    and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
  """;

  test("should return true if note can be formed from magazine text", () {
    expect(harmlessRansomNote(noteText, MagazineText), equals(true));
  });
}

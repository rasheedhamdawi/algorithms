bool isPalindrome(String str) {
  List<String> characters = str.toLowerCase().split('');
  List<String> letters = [];
  RegExp exp = new RegExp(r"[a-z]");

  for (String char in characters) {
    if (exp.hasMatch(char)) letters.add(char);
  }

  return letters.join('') == letters.reversed.join('');
}

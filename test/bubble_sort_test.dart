import "package:test/test.dart";
import "package:algorithms/bubble_sort.dart";

void main() {
  List<int> arr = [5, 8, 9, 6, 7, 3, 2, 1, 4];
  List<int> expected = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  test("should bubble up the largest numbers to the end of the list", () {
    expect(bubbleSort(arr), equals(expected));
  });
}

import "package:test/test.dart";
import "package:algorithms/is_palindrome.dart";

void main() {
  test("should return false if not Palindrome", () {
    expect(isPalindrome('Lorem Ipsum'), equals(false));
  });

  test("should return true if Palindrome", () {
    expect(isPalindrome('Race fast, safe car!'), equals(true));
  });
}

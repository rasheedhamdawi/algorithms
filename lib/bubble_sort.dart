List<int> bubbleSort(List<int> arr) {
  bool swapped;

  do {
    swapped = false;
    for (int i = (arr.length - 1); i > 0; i--) {
      if (arr[i] < arr[i - 1]) {
        int temp = arr[i];
        arr[i] = arr[i - 1];
        arr[i - 1] = temp;
        swapped = true;
      }
    }
  } while (swapped);

  return arr;
}

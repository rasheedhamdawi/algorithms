bool harmlessRansomNote(String noteText, String magazineText) {
  bool noteIsPosible = true;
  List<String> noteArr = noteText.split(' ');
  List<String> magazineArr = magazineText.split(' ');
  Map<String, int> magazineObj = {};

  for (String word in magazineArr) {
    if (!magazineObj.containsKey(word)) {
      magazineObj[word] = 0;
    }

    magazineObj[word]++;
  }

  for (String word in noteArr) {
    if (magazineObj.containsKey(word)) {
      magazineObj[word]--;
      if (magazineObj[word] < 0) {
        noteIsPosible = false;
      }
    } else {
      noteIsPosible = false;
    }
  }

  return noteIsPosible;
}

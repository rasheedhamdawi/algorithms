int binarySearch(List<int> arr, int key) {
  int low = 0;
  int high = arr.length - 1;
  int mid;
  int elm;

  while (low <= high) {
    mid = ((low + high) / 2).floor();
    elm = arr[mid];
    if (elm < key) {
      low = mid + 1;
    } else if (elm > key) {
      high = mid - 1;
    } else {
      return mid;
    }
  }

  return -1;
}
